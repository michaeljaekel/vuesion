import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookie from 'vue-cookie'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import {
  stat
} from 'fs'
import tools from '@/tools/Tools.js'

Vue.use(VueAxios, axios)
Vue.use(VueCookie)
Vue.use(VueMomentJS, moment)

const store = new Vuex.Store({
  state: {
    clock: '00.00.0000 00:00:00',
    fastTimer: 1000,
    slowTimer: 3000,
    standorte: [],
    users: [],
    currentUser: {},
    einsaetze: [],
    currentEinsatz: {},
    matrixData: [],
    miniMatrixData: [{}],
    meldungen: [],
    einsatzTypen: []
  },

  getters: {
    clock(state, getters) {
      return state.clock
    },

    slowTimer(state, getters) {
      return state.slowTimer
    },

    standorte(state, getters) {
      return state.standorte
    },

    users(state, getters) {
      return state.users
    },

    currentUser(state, getters) {
      return state.currentUser
    },

    currentUserId(state, getters) {
      return state.currentUser.id
    },

    einsaetze(state, getters) {
      return state.einsaetze
    },

    openEinsaetze(state, getters) {
      return state.einsaetze.filter(einsatz => einsatz.einsatzStatus == 1)
    },

    currentEinsatz(state, getters) {
      return state.currentEinsatz
    },

    currentEinsatzId(state, getters) {
      var result = VueCookie.get('currentEinsatzId')
      return result
    },

    matrixData(state, getters) {
      return state.matrixData
    },

    filteredMatrix: state => einsatzId => {
      return state.matrixData.filter(
        standort =>
        standort.standort.standortEinsatzId == einsatzId ||
        standort.standort.standortMehrfach == 1
      )
    },

    miniMatrixData(state, getters) {
      return state.miniMatrixData
    },

    miniMatrixDataFilter: state => einsatzId => {
      if (einsatzId != undefined) {
        var result = state.miniMatrixData.filter(function (einsatz) {
          return einsatz.einsatz == einsatzId
        })
        if (result.length != 0) {
          return result[0].standorte
        } else {
          return []
        }
      } else {
        return []
      }
    },

    meldungen(state, getters) {
      return state.meldungen
    },

    filteredMeldungen: state => einsatzId => {
      return state.meldungen.filter(
        meldung =>
        meldung.meldungEinsatzId == einsatzId && meldung.meldungStatus == 1
      )
    },

    einsatzTypen(state, getters) {
      return state.einsatzTypen
    }
  },

  actions: {
    getStandorte({
      commit
    }) {
      return new Promise((resolve, reject) => {
        var link = './static/api/standorte/standorte.php?action=list'
        return axios.get(link).then(
          result => {
            commit('updateStandorte', result.data.records)
            resolve()
          },
          error => {
            this.message({
              message: 'Error by axios: ' + error
            })
            reject()
          }
        )
      })
    },

    getUsers({
      commit
    }) {
      return new Promise((resolve, reject) => {
        var link = './static/api/users/users.php?action=list'
        return axios.get(link).then(
          result => {
            commit('updateUsers', result.data.records)
            resolve()
          },
          error => {
            console.error('Error by axios: ', error)
            reject()
          }
        )
      })
    },

    getEinsaetze({
      commit
    }) {
      return new Promise((resolve, reject) => {
        var link = '/static/api/einsaetze/einsaetze.php?action=list'
        return axios.get(link).then(
          response => {
            commit('updateEinsaetze', response.data.records)
            resolve()
          },
          error => {
            console.error('Error by axios: ', error)
            reject()
          }
        )
      })
    },

    getEinsatzTypen({
      commit
    }) {
      return new Promise((resolve, reject) => {
        var link = './static/api/einsatzTypen/einsatzTypen.php?action=list'
        axios.get(link).then(
          result => {
            commit('updateEinsatzTypen', result.data.records)
            resolve()
          },
          error => {
            console.error('Error by axios: ', error)
            reject()
          }
        )
      })
    },

    setCurrentEinsatzToNone({
      commit
    }) {
      return new Promise((resolve, reject) => {
        VueCookie.delete('currentEinsatzId')
        commit('updateCurrentEinsatz', {})
        resolve()
      })
    },

    setCurrentEinsatz({
      commit
    }, einsatzId) {
      return new Promise((resolve, reject) => {
        var link =
          './static/api/einsaetze/einsaetze.php?action=read&einsatzId=' +
          einsatzId
        return axios.get(link).then(
          result => {
            commit('updateCurrentEinsatz', result.data.record)
            VueCookie.set('currentEinsatzId', result.data.record.id)
            resolve()
          },
          error => {
            console.error('Error by axios: ', error)
            reject()
          }
        )
      })
    },

    setCurrentUser({
      commit
    }, userId) {
      return new Promise((resolve, reject) => {
        var link = './static/api/users/users.php?action=read&userId=' + userId
        axios.get(link).then(
          result => {
            commit('updateCurrentUser', result.data.record)
            VueCookie.set('currentUserId', result.data.record.id)
            resolve()
          },
          error => {
            console.error('Error by Axios: ', error)
            reject()
          }
        )
      })
    },






    // Actions mit Timer-Funktionen
    startClockTimer({
      commit
    }, timerValue) {
      var i = 0;
      var self = this
      setInterval(() => {
        commit('updateClock', moment().format('DD.MM.YYYY HH:mm:ss'))
      }, timerValue)
    },


    startAlarmTimer({
      commit
    }, timerValue) {
      var self = this
      setInterval(() => {
        console.warn("ALARMTIMER")
        self.state.miniMatrixData.forEach((einsatz) => {
          einsatz.standorte.forEach((standort) => {
            var einheiten = standort.einheiten
            einheiten.forEach((einheit) => {
              if (einheit.einheitStatus == 'alarmiert') {
                var a = moment(self.state.clock, "DD-MM-YYYY HH:mm:ss")
                var b = moment(einheit.einheitAlarmzeit, "DD-MM-YYYY HH:mm:ss")
                var minuten = a.diff(b, 'minutes')
                var link = './static/api/einheiten/einheiten.php?action=updateAlarmTime&einheitId=' + einheit.id + '&alarmTime=' + minuten
                axios.get(link).then(() => {
                  if (minuten in {
                      5: '',
                      10: '',
                      15: '',
                    }) {
                    var data = {
                      meldungType: 'warning',
                      meldungText: 'Alarmzeit für die Einheit ' + einheit.einheitButtonName + ' ist überschritten! (' + minuten + ' Minuten)',
                      meldungVon: 'FEZ',
                      meldungAn: 'system',
                      einsatzId: einheit.einheitEinsatzId
                    }
                    tools.insertMeldung(data).then(() => {})
                  }
                }, (error) => {
                  console.error('Fehler beim Update der Alarmzeiten --axio: ', error)
                })
              }
            })
          })
        })
      }, timerValue)
    },




    startMatrixTimer({
      commit
    }, timerValue) {
      var link1 = './static/api/matrix/matrix.php?action=matrixXP'
      var link2 = './static/api/matrix/matrix.php?action=matrixXP4'
      setInterval(() => {
        axios.get(link1).then(
          result => {
            commit('updateMatrixData', result.data.zeilen)
          },
          error => {
            console.error('Error by axios: ', error)
          }
        )
        axios.get(link2).then(
          result => {
            commit('updateMiniMatrixData', result.data.records)
          },
          error => {
            console.error('Error by axios: ', error)
          }
        )
      }, timerValue)
    },

    startEinsaetzeTimer({
      commit
    }, timerValue) {
      var link = './static/api/einsaetze/einsaetze.php?action=list'
      setInterval(() => {
        axios.get(link).then(
          response => {
            commit('updateEinsaetze', response.data.records)
          },
          error => {
            console.error('Error by axios: ', error)
          }
        )
      }, timerValue)
    },

    startMeldungenTimer({
      commit
    }, timerValue) {
      var link = './static/api/meldungen/meldungen.php?action=listAll'
      setInterval(() => {
        axios.get(link).then(
          response => {
            commit('updateMeldungen', response.data.records)
          },
          error => {
            console.error('Error by axios: ', error)
          }
        )
      }, timerValue)
    }
  },

  mutations: {
    updateStandorte(state, data) {
      state.standorte = data
    },

    updateUsers(state, data) {
      state.users = data
    },

    updateCurrentUser(state, data) {
      state.currentUser = data
    },

    updateClock(state, data) {
      state.clock = data
    },

    updateEinsaetze(state, data) {
      state.einsaetze = data
    },

    updateCurrentEinsatz(state, data) {
      state.currentEinsatz = data
    },

    updateMatrixData(state, data) {
      state.matrixData = data
    },

    updateMiniMatrixData(state, data) {
      state.miniMatrixData = data
    },

    updateMeldungen(state, data) {
      state.meldungen = data
    },

    updateEinsatzTypen(state, data) {
      state.einsatzTypen = data
    }
  }
})

export default store
