// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from '@/store/Store'
import feather from 'vue-icon' //https://qinshenxue.github.io/vue-icon/
import './style-daedalus/styles.scss'
//import './styles/styles.scss'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import * as VueGoogleMaps from "vue2-google-maps"
import VueCookie from 'vue-cookie'


Vue.use(VueAxios, axios)
Vue.use(ElementUI, {
  locale,
  size: 'small'
})
Vue.use(feather, 'v-icon')
Vue.use(VueMomentJS, moment)
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBX3WcKYsEv5CtVQgD3vFNhsO_6ZYqTaNg",
    libraries: "places" // necessary for places input
  }
});
Vue.use(VueCookie)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
