import Vue from 'vue'
import store from '@/store/Store.js'
import {
  resolve
} from 'url'

export default new Vue({
  store,
  methods: {
    toFormData: function (obj) {
      var data = new FormData()
      for (var key in obj) {
        data.append(key, obj[key])
      }
      return data
    },

    isEmptyObject(aObject) {
      return Object.keys(aObject).length !== 0
    },

    isUndefined(myVar) {
      return typeof myVar == 'undefined' || myVar == null
    },

    isValue(myVar) {
      return !this.isUndefined(myVar) && this.isEmptyObject(myVar)
    },

    getHumanDate() {
      return this.$moment().format('DD.MM.YYYY')
    },

    getHumanTime() {
      return this.$moment().format('HH:mm:ss')
    },

    getHumanDateTime() {
      return this.$moment().format('DD.MM.YYYY HH:mm:ss')
    },

    getMachineDate() {
      return this.$moment().format('YYYY-MM-DD')
    },

    getMachineTime() {
      return this.$moment().format('HH:mm:ss')
    },

    getMachineDateTime() {
      return this.$moment().format('YYYY-MM-DD HH:mm:ss')
    },

    getFullDateTimeString() {
      return this.$moment().format('ddd MMM DD YYYY HH:mm:ss') + ' GMT'
    },

    insertMeldung(data) {
      return new Promise((resolve, reject) => {
        if (!this.isValue(data.einsatzId)) {
          data.einsatzId = this.$store.getters.currentEinsatz.id
        }
        var newMeldung = {
          meldungType: data.meldungType,
          meldungText: data.meldungText,
          meldungVon: data.meldungVon,
          meldungEinsatzId: data.einsatzId,
          meldungDisponent: this.$store.getters.currentUserId,
          meldungAn: data.meldungAn,
          meldungDatum: this.getHumanDate(),
          meldungZeit: this.getHumanTime(),
          meldungDate: this.getMachineDateTime(),
          meldungStatus: '1'
        }
        newMeldung.meldungType = newMeldung.meldungType.toLowerCase()
        var link = './static/api/meldungen/meldungen.php?action=insert'
        var formData = this.toFormData(newMeldung)

        this.axios.post(link, formData).then(
          result => {
            resolve()
          },
          error => {
            this.$message.error('Error by axios: ', error)
            reject()
          }
        )
      })
    },

    updateMeldung(meldung) {
      var self = this
      return new Promise((resolve, reject) => {
        var formData = self.toFormData(meldung)
        var link = './static/api/meldungen/meldungen.php?action=update'
        this.axios.post(link, formData).then(() => {
          this.$message.info('Meldung aktualsiert')
          resolve()
        }, (error) => {
          this.$message.error('Die Meldung wurde nicht aktualisiert')
          reject()
        })
      })
    }

  }
})
