import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/dashboard/Dashboard'
import Admin from '@/components/admin/Admin'
import Matrix from '@/components/matrix/Matrix'
import ExternMatrix from '@/components/matrix/ExternMatrix'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin
    },
    {
      path: '/matrix',
      name: 'Matrix',
      component: Matrix
    },

    {
      path: '/externmatrix',
      name: 'externMatrix',
      component: ExternMatrix
    }
  ]
})
