<?php session_start . php?>


<?php
/*
ini_set('display_errors', 'On');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
 */
?>


<?php
require_once './../connectDB.php';

try {
    $pdo = new myDBO($configDB);

    $tableName = 'meldungen';
    $keyField = 'id';
    $result = array('error' => false);
    $action = 'list';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    if (isset($_GET['einsatzId'])) {
        $einsatzId = $_GET['einsatzId'];
    }

    if (isset($_GET['meldungId'])) {
        $meldungId = $_GET['meldungId'];
    }

    if ($action == 'update') {
        $query = $pdo->updateQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Update der Meldung';
            $result['query'] = $query;
        }
    }

    if ($action == 'list') {
        $query = "SELECT * FROM meldungen WHERE meldungEinsatzId = '$einsatzId' ORDER BY meldungDate DESC, meldungZeit DESC";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Lesen der Meldung';
            $result['records'] = null;
            $result['query'] = $query;
        }
    }

    if ($action == 'listAll') {
        $query = "SELECT * FROM meldungen ORDER BY meldungDate DESC, meldungZeit DESC";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Lesen der Meldung';
            $result['records'] = null;
            $result['query'] = $query;
        }
    }

    if ($action == 'insert') {
        $query = $pdo->insertQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $lastInsertId = $pdo->lastInsertId();
            $result['error'] = false;
            $result['meldung'] = $_POST['meldungText'];
            $result['newRecordId'] = $lastInsertId;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Could not insert Meldung';
            $result['newRecordId'] = null;
        }
    }

    if ($action == 'read') {
        $query = "SELECT * FROM meldungen WHERE id = '$meldungId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['meldung'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Lesen der Meldung';
            $result['meldung'] = null;
            $result['query'] = $query;
        }
    }

    if ($action == 'delete') {
        $query = "DELETE FROM meldungen WHERE id = '$meldungId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Löschen der Meldung';
            $result['meldung'] = null;
            $result['query'] = $query;
        }
    }

    echo json_encode($result);
    $pdo = null;
} catch (PDOException $e) {
    echo json_encode($e);
    $pdo = null;
};
