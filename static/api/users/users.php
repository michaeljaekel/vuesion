<?php session_start(); ?>

<?php

    require_once('./../connectDB.php');

    try {
        $pdo = new myDBO($configDB);

        $result = array('error' => false);
        $action = 'list';

        $tableName = 'users';
        $keyField = 'id';


        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }

        if (isset($_GET['userId'])) {
            $userId = $_GET['userId'];
        }


        if ($action == 'list') {
            $query = "SELECT * FROM users WHERE userDeleted = false ORDER BY userName ASC";
            $stmt = $pdo -> prepare($query);
            $stmt -> execute();
            $result['records'] = $stmt -> fetchall(PDO::FETCH_ASSOC);
        }


        if ($action == 'read') {
            $query = "SELECT * FROM users WHERE id = '$userId' ORDER BY id";
            $stmt = $pdo -> prepare($query);
            if ($stmt -> execute()) {
                $result['record'] = $stmt -> fetch(PDO::FETCH_ASSOC);
                $result['error'] = false;
                $result['query'] = $query;
                $result['message'] = "Ok";
            } else {
                $result['error'] = true;
                $result['message'] = 'Fehler beim Lesen der User-Daten';
                $result['user'] = null;
            }
        }

        if ($action == 'update') {
            $query = $pdo -> updateQueryFor($tableName, $keyField, $_POST);
            $stmt = $pdo -> prepare($query);
            if ($stmt -> execute()) {
                $result['error'] = false;
                $result['message'] = 'ok';
                $result['query'] = $query;
            } else {
                $result['error'] = true;
                $result['message'] = 'Error: Fehler beim Update der Benutzerdaten';
                $result['query'] = $query;
            }
        }

        if ($action == 'insert') {
            $query = $pdo -> insertQueryFor($tableName, $keyField, $_POST);
            $stmt = $pdo -> prepare($query);
            if ($stmt -> execute()) {
                $lastInsertId = $pdo -> lastInsertId();
                $result['error'] = false;
                $result['message'] = "Anwender eingefügt";
                $result['newRecordId'] = $lastInsertId;
            } else {
                $result['error'] = true;
                $result['message'] = 'Fehler beim Einfügen eines Anwenders';
                $result['newRecordId'] = -1;
            }
        }

        if ($action == 'delete') {
            $query = "UPDATE users SET userDeleted=1 WHERE id='$userId'";
            $stmt = $pdo -> prepare($query);
            if ($stmt -> execute()) {
                $result['error'] = false;
                $result['message'] = "Anwender gelöscht";
                $result['query'] = $query;
            } else {
                $result['error'] = true;
                $result['message'] = 'Fehler beim Löschen eines Anwenders';
                $result['query'] = $query;
            }
        }


        echo json_encode($result);
        $pdo = null;
    } catch (PDOException $e) {
        echo json_encode($e);
        $pdo = null;
    }
