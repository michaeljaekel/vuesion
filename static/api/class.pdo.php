<?php

class myDBO extends PDO {

	private $dbname = null;
	private $user = null;
	private $password = null;
	private $host = null;
	private $port = null;
	private $utf8 = null;


	function __construct($configDB, $utf8=true) {
		$this->dbname = $configDB['dbname'];
		$this->host = $configDB['host'];
		$this->password = $configDB['password'];
		$this->user = $configDB['user'];
		$this->port = $configDB['port'];
		$this->utf8 = $utf8;
		$server = "mysql:host=$this->host; dbname=$this->dbname; port=$this->port";


		$options  	= array (
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		);

		parent::__construct($server, $this->user, $this->password, $options);
		$this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

	}


	function close() {
		PDO::mysql_close($link);
	}

	function updateQueryFor($tableName, $keyField, $values) {
		$keyValue = "";
		$updateSQL = "UPDATE $tableName SET ";
	    foreach ($values as $key=>$val) {
	        if ($key != $keyField) {
				if ($key != 'timestamp') {
					$updateSQL = $updateSQL . "$key='$val',";
				}
	        } else {
				$keyValue = $val;
			}
	    }
	    $updateSQL = substr($updateSQL, 0, -1);
	    $updateSQL = $updateSQL . " WHERE $keyField='$keyValue'";
		echo $updateSQL;
	    return $updateSQL;
	}



	function insertQueryFor($tableName, $keyField, $vars) {
		$queryStr = "INSERT INTO " . $tableName . "(";
		$queryPart2 = "VALUES (";

		$i = 0;
		foreach ($vars as $k => $v) {
			$queryStr .= $k;
			$queryPart2 .= "'" . $v . "'"; ;
			$queryStr .= ",";
			$queryPart2 .= ",";
		} // foreach

		$queryStr = rtrim($queryStr, ",");
		$queryPart2 = rtrim($queryPart2, ",");
		$queryStr .= ") ";
		$queryPart2 .= ") ";

		$queryStr .= $queryPart2;
		return $queryStr;
	}




	function getConfig() {
		$result = array(
			"dbname"	=> $this->dbname,
			"user"		=> $this->user,
			"host"		=> $this->host,
			"password"	=> $this->password,
		);
		return $result;
	}




} // class

?>
