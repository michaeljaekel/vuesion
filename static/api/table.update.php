<?php session_start() ?>
<?php
    ini_set('display_errors', 'off');
    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE );
?>

<?php
    require_once("connectDB.pdo.php");
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $record = $request -> data;
    $tableName  = $request -> tableName;
    $keyField = $request -> keyField;

    $values = $record;

    try {
        $pdo = new myPDO($configDB);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = $pdo->updateQueryFor($tableName, $keyField, $values);
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        $result = array();
        $result['status'] = 200;
        $result['records'] = $query;
        $result['error'] = null;
        echo json_encode($result);
    } catch(PDOException $e) {
        $result['status'] = 409;
        $result['error'] = $e->getMessage();
        $result['records'] = null;
        echo json_encode($result);
        exit();
    }
