<?php session_start();?>



<?php

require_once './../connectDB.php';

try {
    $pdo = new myDBO($configDB);

    $result = array('error' => false);
    $action = 'listAll';
    $tableName = 'einheiten';
    $keyField = 'id';
    $standortId = 1;
    $standortName = '';
    $einheitenPerStandort = 7;

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    if (isset($_GET['standortId'])) {
        $standortId = $_GET['standortId'];
    }

    if (isset($_GET['standortKurzname'])) {
        $standortKurzname = $_GET['standortKurzname'];
    }

    if (isset($_GET['einheitId'])) {
        $einheitId = $_GET['einheitId'];
    }

    if (isset($_GET['alarmTime'])) {
        $alarmTime = $_GET['alarmTime'];
    }

    if (isset($_GET['einsatzId'])) {
        $einsatzId = $_GET['einsatzId'];
    }

    if (isset($_GET['alarmZeit'])) {
        $alarmZeit = $_GET['alarmZeit'];
    }

    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
        if (strLen($filter) != 0) {
            $filterStr = " WHERE einheitRufname LIKE '%" . $filter . "%' OR einheitName LIKE  '%" . $filter . "%'";
        } else {
            $filterStr = '';
        }
    } else {
        $filterStr = '';
    }

    if ($action == 'resetEinheit') {
        //  Nur eine Einheit zurücksetzen.
        $query = "UPDATE einheiten SET einheitAlarmzeit=0, einheitUnixAlarm=0, einheitEinsatzId=0, einheitZF=0, einheitPA=0, einheitGF=0, einheitMA=0, einheitStatus='frei' WHERE einheiten.id = '$einheitId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Einheiten-Reset (System-Reset) fehlgeschlagen';
            $result['error'] = false;
        }
    }

    if ($action == 'resetEinheiten') {
        // Alle Einheiten eines Einsatzs zurücksetzen.
        $query = "UPDATE einheiten SET einheitAlarmzeit=0, einheitUnixAlarm=0, einheitEinsatzId=0, einheitZF=0, einheitPA=0, einheitGF=0, einheitMA=0, einheitStatus='frei' WHERE einheitEinsatzId = '$einsatzId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Einheiten-Reset (System-Reset) fehlgeschlagen';
            $result['error'] = false;
        }
    }

    if ($action == 'einsatzInfo') {
        $query = "SELECT COUNT(*) AS anzahl FROM einheiten WHERE einheiten.einheitEinsatzId = '$einsatzId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['einheitenImEinsatz'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Einsatzinfo mit Fehler';
            $result['statistik'] = [];
            $result['error'] = true;
        }
    }

    if ($action == 'statistik') {
        $query = "SELECT einheiten.einheitEinsatzId AS einsatzId, SUM(einheitZF) AS sumZF, SUM(einheitGF) AS sumGF, SUM(einheitMA) AS sumMA, SUM(einheitPA) AS sumPA FROM einheiten WHERE einheiten.einheitEinsatzId <> 0 AND einheiten.einheitStatus <> 'frei' AND einheiten.einheitStatus <> 'inaktiv' GROUP BY einheiten.einheitEinsatzId ORDER BY einheiten.einheitEinsatzId";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['statistik'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Statisk nicht erfolgreich';
            $result['statistik'] = [];
            $result['error'] = true;
        }
    }

    if ($action == 'alarmStandortEinheiten') {
        $query = "UPDATE einheiten SET einheitStatus='alarmiert', einheitAlarmZeit='$alarmZeit', einheitEinsatzId = '$einsatzId' WHERE einheitStandortId = '$standortId' AND einheitAktiv=9";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Update Alarmierung fehlgeschlagen.';
            $result['error'] = true;
        }
    }

    if ($action == 'updateAlarmTime') {
        $query = "UPDATE einheiten SET einheitUnixAlarm = '$alarmTime' WHERE id = '$einheitId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = "Fehler beim Update der Einheit";
            $result['query'] = $query;
        }
    }

    if ($action == 'alarmtimer') {
        $query1 = "SELECT id, einheitName, einheitStatus, einheitAlarmzeit, einheitUnixAlarm FROM einheiten WHERE einheitStatus = 'alarmiert'";
        $stmt = $pdo->prepare($query1);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $datetime1 = new DateTime($row['einheitAlarmzeit']);
            $datetime2 = new DateTime(date("Y-m-d H:i:s"));
            $diff = $datetime1->diff($datetime2);
            $hours = $diff->format('%h');
            $days = $diff->format('%a');
            $einheitId = $row['id'];
            if (($days > 0) || ($hours > 0)) {
                $minuten = '60+';
            } else {
                $minuten = $diff->format('%i');
            }
            $query2 = "UPDATE einheiten SET einheitUnixAlarm = '$minuten' WHERE id = '$einheitId'";
            $stmt2 = $pdo->prepare($query2);
            $stmt2->execute();
            $result['query1'] = $query1;
            $result['query2'] = $query2;
            $result['message'] = 'Ok';
            $result['error'] = false;
        }
    }

    if ($action == 'resetsystem') {
        $query = "UPDATE einheiten SET einheitAlarmzeit=0, einheitUnixAlarm=0, einheitEinsatzId=0, einheitZF=0, einheitPA=0, einheitGF=0, einheitMA=0, einheitStatus='frei' WHERE einheitImDienst = 1 AND einheitAktiv = 1;";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['query'] = $query;
            $result['message'] = 'Error: Einheiten-Reset (System-Reset) fehlgeschlagen';
            $result['error'] = false;
        }
    }

    if ($action == 'init') {
        $query = "DELETE FROM einheiten WHERE einheitStandortId = $standortId";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            for ($i = 0; $i < $einheitenPerStandort; $i++) {
                $num = $i + 1;
                $einheitName = "einheit #$num @$standortKurzname";
                $query = "INSERT INTO einheiten SET einheitStandortId = '$standortId', einheitName = '$einheitName'";
                $stmt = $pdo->prepare($query);
                if ($stmt->execute()) {
                    $result['query'] = $query;
                    $result['message'] = 'Ok';
                    $result['error'] = false;
                } else {
                    $result['query'] = $query;
                    $result['message'] = 'Fehler beim Lesen der Daten für die Einheiten';
                    $result['error'] = true;
                }
            }
        } else {
            $result['query'] = $query;
            $result['message'] = 'Fehler beim Lesen der Daten für die Einheiten';
            $result['error'] = true;
        }
    }

    if ($action == "delete") {
        $query = "DELETE FROM einheiten WHERE einheitStandortId='$standortId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Löschen der Einheiten';
            $result['query'] = $query;
        }
    }

    if ($action == 'list_by_standort') {
        $query = "SELECT * FROM einheiten WHERE einheitStandortId='$standortId' ORDER BY einheitStandortId, einheiten.id";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['records'] = [];
            $result['query'] = $query;
            $result['message'] = 'Fehler beim Lesen der Daten für die Einheiten';
            $result['error'] = true;
        }
        ;
    }

    if ($action == 'list_by_einsatz') {
        $query = "SELECT id, einheitEinsatzId, einheitStandortId, einheitRufName FROM einheiten WHERE einheitEinsatzId = '$einsatzId' ORDER BY einheitStandortId";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['records'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $result['error'] = false;
            $result['query'] = $query;
        } else {
            $result['records'] = null;
            $result['query'] = $query;
            $result['error'] = true;
            $result['message'] = 'Fehler beim Lesen der Tabelle: $tableName';
        }
    }

    if ($action == 'list') {
        $query = "SELECT * FROM einheiten WHERE einheitStandortId='$standortId' ORDER BY einheitStandortId, einheiten.id";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['records'] = [];
            $result['query'] = $query;
            $result['message'] = 'Fehler beim Lesen der Daten für die Einheiten';
            $result['error'] = true;
        }
        ;
    }

    if ($action == 'update') {
        $query = $pdo->updateQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = "Fehler beim Update der Einheit";
            $result['query'] = $query;
        }
    }

    if ($action == 'listAll') {
        $query = "SELECT einheiten.*, standorte.standortPraefix FROM einheiten LEFT JOIN standorte ON (einheiten.einheitStandortId = standorte.id) $filterStr ORDER BY einheiten.einheitStandortId, einheiten.id;";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
        } else {
            $result['error'] = true;
            $result['message'] = "Fehler beim Lesen der Einheiten-Liste";
            $result['query'] = $query;
            $result['records'] = [];
        }
    }

    if ($action == 'read') {
        $query = "SELECT einheiten.* FROM einheiten WHERE einheiten.id = '$einheitId' ORDER BY id ASC";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['GET'] = $_GET;
            $result['query'] = $query;
            $result['record'] = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $result['error'] = true;
            $result['message'] = "Fehler beim Lesen der Einheiten-Liste";
            $result['query'] = $query;
            $result['record'] = [];
        }
    }

    echo json_encode($result);
    $pdo = null;
} catch (PDOException $e) {
    echo json_encode($e);
    $pdo = null;
}
