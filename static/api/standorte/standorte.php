<?php session_start();?>


<?php

require_once './../connectDB.php';

try {
    $pdo = new myDBO($configDB);

    $result = array('error' => false);
    $action = 'list';
    $standortId = 1;
    $tableName = 'standorte';
    $keyField = 'id';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    if (isset($_GET['standortId'])) {
        $standortId = $_GET['standortId'];
    }

    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
        if (strLen($filter) != 0) {
            $filterStr = " WHERE standortName LIKE '%" . $filter . "%' AND standortDeleted=0";
        } else {
            $filterStr = 'WHERE standortDeleted = 0';
        }
    } else {
        $filterStr = 'WHERE standortDeleted = 0';
    }

    if ($action == 'list') {
        $query = "SELECT * FROM standorte $filterStr ORDER BY standortVGKennung";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['records'] = null;
            $result['query'] = $query;
            $result['message'] = 'Fehler beim Lesen der Standortdaten';
            $result['error'] = true;
        }
    }

    if ($action == 'read') {
        $query = "SELECT * FROM standorte WHERE id = '$standortId' ORDER BY id";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['record'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $result['error'] = false;
        } else {
            $result['records'] = null;
            $result['query'] = $query;
            $result['message'] = 'Fehler beim Lesen der Standortdaten';
            $result['error'] = true;
        }
        ;
    }

    if ($action == 'insert') {
        $query = "INSERT INTO standorte SET standortName='Neuer Standort'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $lastInsertId = $pdo->lastInsertId();
            $result['error'] = false;
            $result['message'] = 'Success: Insert Standort';
            $result['newRecordId'] = $lastInsertId;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Could not insert Standort';
            $result['newRecordId'] = null;
        }
    }

    if ($action == 'update') {
        $query = $pdo->updateQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Update der Standortdaten';
            $result['query'] = $query;
        }
    }

    if ($action == "delete") {
        $query = "UPDATE standorte SET standortDeleted = 1 WHERE id='$standortId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Fehler beim Löschen der Standortdaten';
            $result['query'] = $query;
        }
    }

    if ($action == "checkForFree") {
        $standortId = 27;
        $query = "SELECT COUNT(*) AS einheitenImEinsatz FROM einheiten WHERE einheiten.einheitStandortId = '$standortId' AND einheiten.einheitEinsatzId <> 0 ORDER BY einheiten.einheitStandortId";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row['einheitenImEinsatz'] == 0) {
            //$query2 = "UPDATE standorte SET standortEinsatzId = "
        }

        $result = $imEinsatz['einheitenImEinsatz'];
    }

    echo json_encode($result);
    $pdo = null;
} catch (PDOException $e) {
    echo json_encode($e);
    $pdo = null;
}
