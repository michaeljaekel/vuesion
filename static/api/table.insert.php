<?php session_start() ?>
<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE );
?>

<?php
    require_once("connectDB.pdo.php");
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $record = $request -> data;
    $tableName  = $request -> tableName;
    $keyField = $request -> keyField;

    $values = $record;

    try {
        $pdo = new myPDO($configDB);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = $pdo->insertQueryFor($tableName, $keyField, $values);
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $lastInsertId = $pdo -> lastInsertId();
        $lastId = array(
            "lastId" => $lastInsertId,
        );

        $result = array();
        $result['status'] = 200;
        $result['query'] = $query;
        $result['records'] = $record;
        $result['insertedId'] = $lastInsertId;
        $result['error'] = null;
        echo json_encode($result);
    } catch(PDOException $e) {
        $result['status'] = 409;
        $result['error'] = $e->getMessage();
        $result['records'] = null;
        echo json_encode($result);
        exit();
    }
