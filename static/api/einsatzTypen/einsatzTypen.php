<?php session_start.php ?>


<?php
    /*
    ini_set('display_errors', 'On');
    error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    */
?>


<?php
    require_once('./../connectDB.php');


    try {
        $pdo = new myDBO($configDB);

        $tableName = 'einsatzTypen';
        $keyField = 'id';
        $result = array('error' => false, 'query' => '');
        $action = 'list';


        if (isset($_GET['action'])) {
            $action = $_GET['action'];
        }

        if (isset($_GET['einsatzId'])) {
            $einsatzId = $_GET['einsatzId'];
        }

        if (isset($_GET['meldungId'])) {
            $meldungId = $_GET['meldungId'];
        }


        if ($action == 'list') {
            $query = "SELECT * FROM einsatzTypen ORDER BY einsatzType;";
            $stmt = $pdo -> prepare($query);
            if ($stmt -> execute()) {
                $result['records'] = $stmt -> fetchAll(PDO::FETCH_ASSOC);
            } else {
                $result['records'] = null;
                $result['error'] = "Datenbankfehler bei $tableName";
                $result['query'] = $query;
            }
        }


        echo json_encode($result);
        $pdo = null;
    } catch (PDOException $e) {
        echo json_encode($e);
        $pdo = null;
    };