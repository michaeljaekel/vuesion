<?php session_start();?>


<?php

ini_set('display_errors', 'On');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

require_once './../connectDB.php';

try {
    $pdo = new myDBO($configDB);

    $result = array('error' => false);
    $action = 'list';
    $standortId = 1;
    $tableName = 'einsaetze';
    $keyField = 'id';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    if ($action == 'list') {
        $query = "SELECT einsaetze.id, einsaetze.*, users.userKuerzel FROM einsaetze LEFT JOIN users ON (einsaetze.einsatzDisponent = users.id) ORDER BY einsatzDatum, einsatzUhrZeit";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['records'] = $stmt->fetchall(PDO::FETCH_ASSOC);
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['records'] = [];
            $result['query'] = $query;
            $result['message'] = "Fehler beim Lesen der Tabelle '$tablename'";
            $result['error'] = true;
        }
        ;
    }

    if ($action == 'read') {
        if (isset($_GET['einsatzId'])) {
            $einsatzId = $_GET['einsatzId'];
        }
        $query = "SELECT * FROM einsaetze WHERE id = '$einsatzId'";
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['record'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $result['query'] = $query;
            $result['message'] = 'Ok';
            $result['error'] = false;
        } else {
            $result['record'] = [];
            $result['query'] = $query;
            $result['message'] = "Fehler beim Lesen der Tabelle '$tablename'";
            $result['error'] = true;
        }
        ;
    }

    if ($action == 'update') {
        $query = $pdo->updateQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $result['error'] = false;
            $result['message'] = 'ok';
            $result['query'] = $query;
        } else {
            $result['error'] = true;
            $result['message'] = "Fehler beim Update der Tabelle: '$tableName'";
            $result['query'] = $query;
        }
    }

    if ($action == 'insert') {
        $query = $pdo->insertQueryFor($tableName, $keyField, $_POST);
        $stmt = $pdo->prepare($query);
        if ($stmt->execute()) {
            $lastInsertId = $pdo->lastInsertId();
            $result['error'] = false;
            $result['newRecordId'] = $lastInsertId;
        } else {
            $result['error'] = true;
            $result['message'] = 'Error: Could not insert Einsatz';
            $result['newRecordId'] = null;
        }
    }

    echo json_encode($result);
    $pdo = null;
} catch (PDOException $e) {
    echo json_encode($e);
    $pdo = null;
}