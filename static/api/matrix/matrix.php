<?php session_start();?>



<?php
require_once './../connectDB.php';

try {
    $pdo = new myDBO($configDB);

    $result = array('error' => false);
    $action = 'matrixXP2';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    if (isset($_GET['einsatzId'])) {
        $einsatzId = $_GET['einsatzId'];
    }

    if ($action == 'matrixXP') {
        $query1 = "SELECT standorte.id, standorte.standortKurzname, standorte.standortPraefix, max(einheiten.einheitEinsatzId) AS generalEinsatzId, einheiten.einheitEinsatzId AS einsatzId, standortGruppenalarm, standortMehrfach FROM standorte LEFT JOIN einheiten ON(standorte.id = einheiten.einheitStandortId) GROUP BY standorte.id ORDER BY standorte.standortGruppe, standorte.standortKurzname";
        $stmt = $pdo->prepare($query1);
        $stmt->execute();
        $i = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $standortId = $row['id'];
            $einheitenImEinsatz = 0;
            $name = $row['standortKurzname'];
            $result['zeilen'][$i]['standort'] = $row;

            $query2 = "SELECT * FROM einheiten WHERE einheitStandortId = '$standortId'";
            $stmt2 = $pdo->prepare($query2);
            $stmt2->execute();
            $result['zeilen'][$i]['einheiten'] = $stmt2->fetchall(PDO::FETCH_ASSOC);

            $query3 = "SELECT COUNT(*) AS anzahl FROM einheiten WHERE einheitStandortId = '$standortId' AND einheitEinsatzId <> 0 and einheitUnixAlarm > 5";
            $stmt3 = $pdo->prepare($query3);
            $stmt3->execute();
            $result['zeilen'][$i]['einheitenImEinsatz'] = $stmt3->fetch(PDO::FETCH_ASSOC);
            $i++;
        }
        $result['query1'] = $query1;
        $result['query2'] = $query2;
        echo json_encode($result);
    }

    if ($action == 'matrixXP4') {
        $query1 = "SELECT id AS einsatzId FROM einsaetze WHERE einsatzStatus = 1 ORDER by id";
        $stmt1 = $pdo->prepare($query1);
        $stmt1->execute();
        $records = [];
        while ($row1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
            $record = [];
            $currentEinsatzId = $row1['einsatzId'];
            $einsatz = array(
                'einsatzId' => $currentEinsatzId,
            );

            $query2 = "SELECT standorte.id AS standortId, standorte.standortKurzname FROM standorte LEFT JOIN einheiten ON (einheiten.einheitStandortId = standorte.id) WHERE einheitEinsatzId = $currentEinsatzId GROUP BY standorte.id";
            $stmt2 = $pdo->prepare($query2);
            $stmt2->execute();
            $standorte = [];
            while ($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                $currentStandortId = $row2['standortId'];

                $query3 = "SELECT id, einheitName, einheitButtonName, einheitEinsatzId, einheitStandortId, einheitStatus, einheitZF, einheitGF, einheitMA, einheitPA, einheitAktiv, einheitAlarmzeit, einheitUnixAlarm FROM einheiten WHERE einheiten.einheitEinsatzId = $currentEinsatzId AND einheitStandortId= $currentStandortId";
                $stmt3 = $pdo->prepare($query3);
                $stmt3->execute();
                $einheiten = $stmt3->fetchAll(PDO::FETCH_ASSOC);
                $numRows = sizeof($einheiten);
                $standort = array(
                    'standortName' => $row2['standortKurzname'],
                    'standortId' => $row2['standortId'],
                    'einheiten' => $einheiten,
                    'numEinheiten' => $numRows,
                );
                $standorte[] = $standort;
            }

            $record = array(
                'einsatz' => $currentEinsatzId,
                'standorte' => $standorte,
            );

            $records[] = $record;
        }

        $result['records'] = $records;
        echo json_encode($result);
    }

} catch (PDOException $e) {
    echo json_encode($e);
    $pdo = null;
}

?>
